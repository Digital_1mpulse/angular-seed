/**
 * App.js initial setup. Routing was moved inside controllers.
 */
(function () {
    'use strict';

    angular.module('angular-seed', [
        'ui.router',
        'webicon'
    ])
    .config(configureWebIcons)
    .controller('ApplicationController', ApplicationController);

    //An App controller that changes some of the bounded data in the view if they are not set.
    ApplicationController.$inject = ['$scope', '$anchorScroll', '$rootScope'];
    function ApplicationController($scope, $anchorScroll, $rootScope) {
        var app = this;

        $scope.$on('$stateChangeSuccess', onStateChangeSuccess);

        function onStateChangeSuccess(event, toState, toParams, fromState, fromParams) {

        }
    }

    function configureWebIcons($webiconProvider){
        $webiconProvider
            .svgSet('md', '/images/icons/material-icons.svg');
    }

})();