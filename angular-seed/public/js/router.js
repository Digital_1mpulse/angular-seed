(function () {
    'use strict';

    angular.module('angular-seed')
        .config(config);

    function config($urlRouterProvider) {
        $urlRouterProvider
            .otherwise('/');
    }

})();