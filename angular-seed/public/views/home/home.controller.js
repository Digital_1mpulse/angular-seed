/**
 * Home Controller.
 */
(function() {
    'use strict';

    angular.module('angular-seed')
        .config(configRouting)
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope'];

    function HomeController($scope) {
        var home = this;

        home.hello = "Home";
    }

    function configRouting($stateProvider){
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'views/home/home.html',
                controller: 'HomeController as home'
            });
    }
})();