'use strict';

var gulp = require('gulp'),
    gulpLoadPlugins = require('gulp-load-plugins'),
    $ = gulpLoadPlugins(),

    browserSync = require('browser-sync').create(),
    del = require('del'),
    gutil = require('gulp-util'),
    reload = browserSync.stream,
    runSequence = require('run-sequence');

//Environment variables
var paths = {};

/**
 * Task to run when developing locally. Will clean the build folder, and run the js, vendor, html, sass, and images
 * tasks. It will then connect to live reload, and start watching the sass, html, js, and images directory.
 */
gulp.task('local', ['build', 'watch']);

/**
 * Task to clean then run the js, vendor, html, sass, and images tasks individually.
 */
gulp.task('build', function(done) {
    runSequence(['clean', 'set-paths'],
                ['defaultjs', 'fonts', 'html', 'icons', 'images', 'js', 'sass'],
                done);
});

/**
 * Sets destination paths.
 */
gulp.task('set-paths', function() {
    var baseDestPath = 'build/public/';
    paths = {
        src: {
            defaultjs: ['public/js/default/**/*.js'],
            fonts: ['public/fonts/**'],
            html: ['public/**/*.html'],
            icons: ['public/icons/**'],
            images: ['public/images/**'],
            js: ['public/**/*.js', 'public/js/vendor/**/*.js', '!public/js/default/**'],
            sass: ['public/**/*.scss', '!public/js/default/**']
        },
        dest: {
            css: baseDestPath + 'css/',
            defaultjs: baseDestPath + 'js/default',
            fonts: baseDestPath + 'fonts',
            html: baseDestPath,
            icons: baseDestPath + 'icons/',
            images: baseDestPath + 'images/',
            js: baseDestPath
        }
    };
});

/**
 * Task to clean the build folder.
 */
gulp.task('clean', function() {
    return del(['build/**/*']);
});

gulp.task('defaultjs', function() {
    return gulp.src(paths.src.defaultjs)
        .pipe(gulp.dest(paths.dest.defaultjs))
        .pipe(reload());
});

gulp.task('fonts', function() {
    return gulp.src(paths.src.fonts)
        .pipe(gulp.dest(paths.dest.fonts))
        .pipe(reload());
});

/**
 * Task to reload html files.
 */
gulp.task('html', function() {
    gulp.src(paths.src.html)
        .pipe(gulp.dest(paths.dest.html))
        .pipe(reload());
});

/**
 * Task to reload icons.
 */
gulp.task('icons', function() {
    return gulp.src(paths.src.icons)
        .pipe(gulp.dest(paths.dest.icons))
        .pipe(reload());
});

/**
 * Task to reload image files.
 */
gulp.task('images', function() {
    gulp.src(paths.src.images)
        .pipe(gulp.dest(paths.dest.images))
        .pipe(reload());
});

/**
 * Task to reload javascript files.
 */
gulp.task('js', function() {
    gulp.src(paths.src.js)
        .pipe(gulp.dest(paths.dest.js))
        .pipe(reload());
});

/**
 * Task to reload sass files.
 */
gulp.task('sass', function() {
    gulp.src(paths.src.sass)
        .pipe($.plumber())
        .pipe($.sourcemaps.init())
        // Run the Sass preprocessor, include paths to vendor folders, and log any errors
        .pipe($.sass({
            includePaths: ['./public/js/default'],
            errLogToConsole: true
        }))
        // Prefix CSS attributes automatically to support the last 3 versions of each browser
        .pipe($.autoprefixer({ browsers: ['last 3 version'] }))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(paths.dest.css))
        .pipe(reload());
});

/**
 * Task to reload vendor files.
 */
gulp.task('vendor', function() {
    return gulp.src(paths.src.vendor)
        .pipe(gulp.dest(paths.dest.vendor))
        .pipe(reload());
});

/**
 * Task to watch for changes in these directories.
 */
gulp.task('watch', ['build'], function() {
    browserSync.init({
        server: './build/public'
    });

    gulp.watch(paths.src.html, ['html']);
    gulp.watch(paths.src.fonts, ['fonts']);
    gulp.watch(paths.src.images, ['images']);
    gulp.watch(paths.src.js, ['js']);
    gulp.watch(paths.src.defaultjs, ['defaultjs']);
    gulp.watch(paths.src.sass, ['sass']);
});