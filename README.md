# README #

### What is this repository for? ###

* Angular Seed
* Version 1.0.0

### How do I get set up? ###

1. Install git, and add to your PATH environment variable.
2. Install node.
3. Install gulp using **npm install gulp -g**.
4. Install bower using **npm install bower -g**.
5. Run **npm install** in your project directory.
5. Run **bower install** in your project directory.
5. Run **gulp local** and access the site via localhost:3000

### Things to Know ###

1. You can add new icon sets within app.js.
2. Routing is done within each individual controller. The router.js file exists solely for the .otherwise routing.

### Contribution guidelines ###


### Who do I talk to? ###

* Rey Garcia - **admin**